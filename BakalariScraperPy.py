#encoding: utf-8
# pyinstaller --add-data="assets/*;assets/" -i"assets/icon.ico" --noconsole main.py
import sys, os, winreg, scraper, ui
from PyQt5.QtCore import Qt, QTimer
from PyQt5.QtWidgets import QLabel, QMessageBox
from PyQt5.QtGui import QIcon
from datetime import datetime, timedelta
from win10toast import ToastNotifier
from pyshortcuts import make_shortcut

def getCurrentColumnFromDate():
    now = datetime.now()
    
    start = now.replace(minute=5, hour=7) - timedelta(minutes=55)
    for i in range(0,10):
        toAdd = 55
        if(i == 3):
            toAdd = 65
        start = start + timedelta(minutes=toAdd)
        end = start + timedelta(minutes=45)
                                
        if(start <= now <= end):
            
            progress = ((end-now).total_seconds()) / ((end-start).total_seconds())
            
            obj = lambda: None
            obj.celln = i
            obj.progress = 1 - round(progress,2)
            return obj
    
    return None

rows = []
app = None
oldHash = None
notifier = ToastNotifier()
appName = "BakalariScraperPy"
path = sys.argv[0]
dir = os.path.dirname(path)

def loadData():
    # print("LOADING DATA")
    global rows, oldHash
    s = scraper.Scraper()
    rows = s.scrape()
    
    newHash = scraper.calculateHash(rows)
    
    if oldHash != newHash:
        if oldHash != None:
            sendNofitication("Aktualizace v rozvrhu")
        oldHash = newHash
        
        
def sendNofitication(text):
    global notifier
    notifier.show_toast(appName, text, duration=10, threaded=True, icon_path='assets/icon.ico')

def rewrite():
        
    global app, rows
    
    c = getCurrentColumnFromDate()
    day = datetime.now().weekday()

    for rown, row in enumerate(rows):
        
        for celln, cell in enumerate(row.cells):
            item = QLabel(cell.getText())
            item.setAlignment(Qt.AlignCenter)
                                    
            if (c != None) and (celln == c.celln) and (rown == day):
                item.setStyleSheet('background: qlineargradient(x1:0, y1:0, x2:1, y2:0,stop:0 rgba(0, 120, 215, 0.8), stop:{0} rgba(0, 120, 215, 0.6), stop:{1} white)'.format(c.progress, c.progress+0.01))
                # item.setStyleSheet('border: 3px solid #0078D7;')
            elif cell.type == "removed":
                # item.setStyleSheet('background: #d6787f; color: white')
                item.setStyleSheet('background: qlineargradient(x1:0, y1:0, x2:1, y2:1,stop:0 #ca515a, stop:1 #d6787f); color: white')
            elif cell.type == "changed":
                item.setStyleSheet('background: qlineargradient(x1:0, y1:0, x2:1, y2:1,stop:0 rgb(0, 120, 215), stop:1 rgba(0, 120, 215, 0.7)); color: white')
                                
            item.setTextFormat(Qt.RichText)
            app.tableWidget.setCellWidget(rown, celln, item)
            if cell.type == "changed":
                app.tableWidget.cellWidget(rown, celln).setToolTip('<div style="color:black" >{0}</div>'.format(cell.changeInfo.split(":",1)[0]))

#show accept dialog using pyqt5, if accepted then do something
def showAcceptDialog():
    msg = QMessageBox()
    msg.setWindowIcon(QIcon('assets/icon.ico'))
    msg.setText("Chcete nastavit, aby se aplikace spustila při startu systému?")
    msg.setWindowTitle(appName)    
    msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
    buttonY = msg.button(QMessageBox.Yes)
    buttonY.setText('Ano')
    buttonN = msg.button(QMessageBox.No)
    buttonN.setText('Ne')
    returnValue = msg.exec_()
    if returnValue == QMessageBox.Yes:
        runAtStartup()
        
def runAtStartup():
    key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Run', 0, winreg.KEY_ALL_ACCESS)
    winreg.SetValueEx(key, appName, 0, winreg.REG_SZ, path)
    winreg.CloseKey(key)

def isRunAtStartup():
    try:
        key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Run', 0, winreg.KEY_ALL_ACCESS)
        winreg.QueryValueEx(key, appName)
        return True
    except:
        return False
    
def removeFromStartup():
    key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, r'Software\Microsoft\Windows\CurrentVersion\Run', 0, winreg.KEY_ALL_ACCESS)
    winreg.DeleteValue(key, appName)
    winreg.CloseKey(key)
    
#check if app is exe file
def isExecutable():
    return path.endswith(".exe")

def main():
    
    global rows, app
    
    sendNofitication("Brrrrrrr")
    app = ui.ex
        
    loadData()
    rewrite()
    
    if(isExecutable() and (not isRunAtStartup())):
        make_shortcut('\\', executable=path, name='BakalariScraperPy', icon=dir + r"/assets/icon.ico", terminal=False)
        showAcceptDialog()
    
    dataTimer = QTimer()
    dataTimer.timeout.connect(loadData)
    dataTimer.start(300000)  
    
    rewriteTimer = QTimer()
    rewriteTimer.timeout.connect(rewrite)
    rewriteTimer.start(5000)  
    
    ui.execute()


if __name__ == '__main__':
    main()