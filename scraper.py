#encoding: utf-8
import json, requests, collections, hashlib, datetime
from wsgiref import headers
from bs4 import BeautifulSoup
collections.Callable = collections.abc.Callable

class Row:
    def __init__(self, soupRow, group):
        fullDate = soupRow.select(".bk-day-wrapper > div > span")
        self.date = fullDate[0].text
        self.day = fullDate[1].text
        self.cells = []

        for cell in soupRow.find_all("div", class_="bk-timetable-cell"):
            metas = cell.select('div[data-detail]')
            
            c = Cell(None)
            
            if len(metas) == 0:
                c = Cell(None)
            elif len(metas) == 1:
                c = Cell(metas[0])
                
                if not ((c.type == "removed") or (c.type == "blank")):
                    if not ((c.group == "") or (c.group == group)):
                        c = Cell(None)
            else:
                for meta in metas:
                    cell = Cell(meta)
                    if(cell.group == "") or (cell.group == group):
                        c = cell
                        break        
                            
            self.cells.append(c)
            
class Cell:
    
    def __init__(self, meta):
        
        if(meta == None):
            self.type = "blank"
            return
        
        self.meta = json.loads(meta.attrs["data-detail"])
        self.type = self.meta["type"]
        
        if(self.type == "removed"):
            return
        
        if(self.meta["changeinfo"] != ""):
            self.changeInfo = self.meta["changeinfo"]
            self.type = "changed"
        
        self.subject = self.meta["subjecttext"].split(" |")[0]
        self.subjectShort = getText(meta.find(class_="middle"))
        self.teacher = self.meta["teacher"]
        self.teacherShort = getText(meta.find(class_="bottom"))
        self.group = self.meta["group"]
        self.room = self.meta["room"]
                
    def getText(self):
        if self.type == "removed": return ""
        elif self.type == "blank": return ""
        else: return '<span style="font-size: 20px;font-weight: bold;">{0}</span><br><span style="font-size: 14px;">{1}</span>'.format(self.subjectShort, self.room) 

def getText(soupObject):
    return "".join(soupObject.text.split())


class Scraper:
    URL = "https://is.sps-prosek.cz/app/Timetable/Public/"
    permanentUrl = URL + "Permanent/Class/" 
    actualUrl = URL + ("Next/Class/" if datetime.datetime.today().weekday() > 5  else "Actual/Class/")
    className = "3ITB"
    group = "S2"
    classCode = None
    headers = {'Accept-Language': 'cs,en-US;q=0.7,en;q=0.3'}


    def __init__(self, className = className, group = group, url = URL):
        self.URL = url
        self.className = className
        self.group = group
        page = requests.get(self.URL, headers=self.headers)
        soup = BeautifulSoup(page.content, "html.parser")

        for option in soup.find(id = "selectedClass").find_all("option"):
            if(getText(option) == self.className):
                self.classCode = option.attrs["value"]
                break
            
        if(self.classCode == None):
            print("Skupina s tímto názvem neexistuje")
            exit()
        

    def scrape(self):
        page = requests.get(self.permanentUrl + self.classCode, headers=self.headers)
        soup = BeautifulSoup(page.content, "html.parser")
        soupRows = soup.find_all("div", class_="bk-timetable-row")
        permamentRows = []
        
        for soupRow in soupRows:
            permamentRows.append(Row(soupRow, self.group))

        page = requests.get(self.actualUrl + self.classCode, headers=self.headers)
        soup = BeautifulSoup(page.content, "html.parser")
        soupRows = soup.find_all("div", class_="bk-timetable-row")
        rows = []

        for soupRow in soupRows:
            rows.append(Row(soupRow, self.group))
            
        for rown, row in enumerate(rows):
            for celln, cell in enumerate(row.cells):
                if permamentRows[rown].cells[celln].type == "blank" and rows[rown].cells[celln].type == "removed":
                    rows[rown].cells[celln] = Cell(None)
            
        return rows;

def calculateHash(rows):
    serialized = ""
    for row in rows:
        for cell in row.cells:
            # serialize cell [type, subjectShort, teacherShort, room] if type not equals "blank" or if type equals "removed" serialize only [type]
            if cell.type != "blank":
                serialized += cell.type
                if cell.type != "removed":
                    serialized += cell.subjectShort + cell.teacherShort + cell.room
                    
    return hashlib.sha256(serialized.encode('utf-8')).hexdigest()