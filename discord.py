#encoding: utf-8
from ast import arg
from multiprocessing import set_forkserver_preload
import sched
import time
import json
from numpy import argsort
import scraper
import hashlib
from table2ascii import table2ascii, PresetStyle
from discord_webhook import DiscordWebhook

def sendToDiscord(content, url):
    webhook = DiscordWebhook(
        url=url, 
        content=content)
    response = webhook.execute()
    print(response)

def discordController(sc, hash, args):
    print("discordController called")
    s = scraper.Scraper(args["className"], args["group"])
    rows = s.scrape()
    content = []
    for rown, row in enumerate(rows):
        rowArr = []
        rowArr.append(row.day)
        for celln, cell in enumerate(row.cells):
            if cell.type != "removed" and cell.type != "blank" and cell.type != "changed":
                rowArr.append(cell.subjectShort+"\n."+cell.room)
            elif cell.type == "removed":
                rowArr.append("[X]")
            elif cell.type == "changed":
                rowArr.append(" ["+cell.subjectShort+"] \n."+cell.room)
            else:
                rowArr.append("")
        content.append(rowArr)


    output = table2ascii(
        header=["", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
        body=content,
        style=PresetStyle.thin_rounded,
        first_col_heading=True
    )
    newHash = hashlib.sha512(output.encode('utf-8')).hexdigest()
    if hash != newHash:
        header = "Class: "+args["className"]+" Group: "+args["group"]
        sendToDiscord("```css\n"+header+"\n"+ output +"\n```", args["webhook"])\

    sc.enter(100, 1, discordController, (sc, newHash, args))



def main():
    #loop through discord_config.json
    sc = sched.scheduler(time.time, time.sleep)
    with open('discord_config.json') as json_file:
        data = json.load(json_file)
        for config in data:
            args = config
            print(args)
            sc.enter(0, 1, discordController, (sc, "", args))
            
    sc.run()


if __name__ == '__main__':
    main()
