#encoding: utf-8
import sys
from PyQt5.QtWidgets import QDesktopWidget, QWidget, QVBoxLayout, QTableWidget, QAbstractItemView, QApplication, QHeaderView
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
                    
   
#Main Window
class App(QWidget):
    def __init__(self):
        super().__init__()
        self.setFocus()
        # app.focusChanged.connect(self.focusChange)
        self.title = 'BakalariScraperPy'
        self.setWindowIcon(QIcon('assets/icon.ico'))
        self.left = 0
        self.top = 0
        self.width = 1500
        self.height = 500
   
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())
        
        # flags = Qt.WindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        # self.setWindowFlags(flags)
   
        self.createTable()
   
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tableWidget)
        self.setLayout(self.layout)
        self.setFocusPolicy(Qt.StrongFocus)
           
        #Show window
        self.show()
        
    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            self.close() 
            
    def focusChange(self):
        if not self.isActiveWindow():
            self.close()
   
    #Create table
    def createTable(self):
        self.tableWidget = QTableWidget()
        self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
  
        #Row count
        self.tableWidget.setRowCount(5) 
        
        # self.tableWidget.setStyleSheet("background: #181818;")
  
        #Column count
        self.tableWidget.setColumnCount(10)          
        horizontalLabels = []
        horizontalLabels.append("0\n07:05 - 7:50")
        horizontalLabels.append("1\n8:00 - 8:45")
        horizontalLabels.append("2\n8:55 - 9:40")
        horizontalLabels.append("3\n10:00 - 10:45")
        horizontalLabels.append("4\n10:55 - 11:40")
        horizontalLabels.append("5\n11:50 - 12:35")
        horizontalLabels.append("6\n12:45 - 13:30")
        horizontalLabels.append("7\n13:40 - 14:25")
        horizontalLabels.append("8\n14:35 - 15:20")
        horizontalLabels.append("9\n15:30 - 16:15")
        
        self.tableWidget.setHorizontalHeaderLabels(horizontalLabels)
        self.tableWidget.setVerticalHeaderLabels("Po,Ut,St,Ct,Pa".split(','))
        #Table will fit the screen horizontally
        self.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.tableWidget.horizontalHeader().setSectionResizeMode(
            QHeaderView.Stretch)
        self.tableWidget.verticalHeader().setStretchLastSection(True)
        self.tableWidget.verticalHeader().setSectionResizeMode(
            QHeaderView.Stretch)
        
        self.tableWidget.setSelectionMode(QAbstractItemView.NoSelection)
        self.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        
        self.tableWidget.setWordWrap(True)
        
        
app = QApplication(sys.argv)
ex = App()   

def execute():
    app.exec_()
